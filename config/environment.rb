# Load the Rails application.
require File.expand_path('../application', __FILE__)

module Rails

  def self.get_custom_logger

    if @custom_logger == nil
      @custom_logger = Logger.new Rails.root.join('log', 'development.log')
      @custom_logger.formatter = Logger::Formatter.new
      Rails.logger = @custom_logger
      ActiveRecord::Base.logger = @custom_logger
      @custom_logger.info 'Custom logger created!'
    end

    @custom_logger
  end

  def self.time_comsumed

    time_consumed = Benchmark.measure {
      Rails.get_custom_logger.debug('Starting block execution')
      yield if block_given?
      Rails.get_custom_logger.debug('Block execution finished')
    }

    Rails.get_custom_logger.debug("first_name, time consumed:  #{time_consumed.to_s} seconds ")
    time_consumed
  end
end


Rails.get_custom_logger.info 'Starting custom logger'

# Initialize the Rails application.
Rails.application.initialize!
